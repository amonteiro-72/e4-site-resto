<?php

include_once "bd.inc.php";

function getAimerByMailU($mailU) {

	// A compléter - question 2.1 
     $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select resto.* from site_mvc.resto inner join site_mvc.aimer on aimer.id_r = resto.id where aimer.mail = :mailU");
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur ! : " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getAimerByIdR($idR) {
     $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select aimer.* from site_mvc.resto inner join site_mvc.resto on aimer.id_r = resto.id where aimer.id_r = :idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_STR);
        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur ! : " . $e->getMessage();
        die();
    }
    return $resultat;

	// A compléter - question 2.1
}

function getAimerById($mailU, $idR){
    
	// A compléter - question 2.1
     $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select resto.* from site_mvc.resto inner join site_mvc.aimer on aimer.id_r = resto.id where aimer.mail = :mailU and aimer.id_r = :idR");
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
        
    } catch (PDOException $e) {
        print "Erreur ! : " . $e->getMessage();
        die();
    }
    return $resultat;
}

function addAimer($mailU, $idR) {
        
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into site_mvc.aimer values(  :idR , :mailU ) ");
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->bindValue(':idR', $idR, PDO::PARAM_STR);
        $temp= $req->execute();
        
        
        
    } catch (PDOException $e) {
        print "Erreur ! : " . $e->getMessage();
        die();
    }
    return $temp;
	// A compléter - question 2.2
}

function delAimer($mailU,$idR){
    try
    {
        $cnx = connexionPDO();
        $req = $cnx->prepare("delete from site_mvc.aimer where id_r= :idR and mail= :mailU");
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->bindValue(':idR', $idR, PDO::PARAM_STR);
        $temp= $req->execute();
        
                
    }
 catch (PDOException $e)
 {
     print "Erreur ! : " . $e->getMessage();
     die();
 }
    
    }
        


?>
