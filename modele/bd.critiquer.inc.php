<?php

include_once "bd.inc.php";

function getCritiquerByIdR($idR) {
    $resultat = array();
    
    // completer le code manquant (question 3.3)
    try {
            $cnx = connexionPDO();
            $req = $cnx->prepare("select critiquer.* from site_mvc.critiquer where id_r= :idR ");
            $req->bindValue(':idR',$idR, PDO::PARAM_INT);
            $req->execute();
            
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
            while ($ligne) {
                $resultat[] = $ligne;
                $ligne = $req->fetch(PDO::FETCH_ASSOC);
            }
            
            
        } catch (PDOException $e) {
            print "Erreur ! : " . $e->getMessage();
            die();

        }

    return $resultat;
    
    
   
}

function getNoteMoyenneByIdR($idR) {
        $resultat = array();
        
        try {
            $cnx = connexionPDO();
            $req = $cnx->prepare("select avg(critiquer.note) from site_mvc.critiquer where id_r = :idR ");
            $req->bindValue(':idR',$idR, PDO::PARAM_INT);
            $req->execute();
            
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
            while ($ligne) {
                $resultat[] = $ligne;
                $ligne = $req->fetch(PDO::FETCH_ASSOC);
            }
            
            
        } catch (PDOException $e) {
            print "Erreur ! : " . $e->getMessage();
            die();

        }
        if ($ligne ==0)
        {
            $resultat=0;
        }
        return $resultat;
	// compléter le code manquant (question 1)
}


?>
